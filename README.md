## Synopsis

This project uses **ant-based clustering** to cluster textual data. The weighted average entropy and number of clusters are used to compare the quality of the solutionto the k-means method and agglomerative hierarchical clustering. In addition, the heterogeneous agents model is examined, as we compare a fast ant dominated system to a slow ant dominated system on these same criteria.
\

## Motivation

The project is the main assignment for the **Advanced Self-Organization** course.

## Installation

The algorithm is implemented into the Weka data mining framework. To run, build the Java project in an IDE to obtain the necessary packages. The dataset can be obtained online, the processed dataset is included in this repository.

## Tests

To run, select the included dataset in Weka, and select the ant clusterer in the clusterers drop-down menu.

## Contributors

Authors are Dylan Goldsborough, dylan.goldsborough@student.uva.nl at University of Amsterdam, and Geert Konijnendijk, geert.konijnendijk@student.uva.nl at University of Amsterdam.

## License

No license.