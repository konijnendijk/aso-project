package weka.core;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Vector;

import weka.core.neighboursearch.PerformanceStats;

public class JaccardDistance
		implements DistanceFunction, Serializable {

	private Instances data;
	private Range range = new Range();

	@Override
	public Enumeration listOptions() {
		return new Vector<>().elements();
	}

	@Override
	public void setOptions(String[] options) throws Exception {
	}

	@Override
	public String[] getOptions() {
		return new String[0];
	}

	@Override
	public void setInstances(Instances insts) {
		this.data = insts;
	}

	@Override
	public Instances getInstances() {
		return data;
	}

	@Override
	public void setAttributeIndices(String value) {
		range.setRanges(value);

	}

	@Override
	public String getAttributeIndices() {
		return range.getRanges();
	}

	@Override
	public void setInvertSelection(boolean value) {
		range.setInvert(value);

	}

	@Override
	public boolean getInvertSelection() {
		return range.getInvert();
	}

	@Override
	public double distance(Instance first, Instance second) {
		return distance(first, second, null);
	}

	@Override
	public double distance(Instance first, Instance second, PerformanceStats stats) {
		return distance(first, second, Double.POSITIVE_INFINITY, stats);
	}

	@Override
	public double distance(Instance first, Instance second, double cutOffValue) {
		return distance(first, second, cutOffValue, null);
	}

	@Override
	public double distance(Instance first, Instance second, double cutOffValue, PerformanceStats stats) {
		double TaTa = 0;
		double TaTb = 0;
		double TbTb = 0;
		for (int i = 0; i < first.numAttributes(); i++) {
			Attribute a = first.attribute(i);
			// Only use numeric attributes
			if (a.isNumeric()) {
				TaTb = TaTb + second.value(i) * first.value(i);
			}
		}
		for (int i = 0; i < first.numAttributes(); i++) {
			Attribute a = first.attribute(i);
			// Only use numeric attributes
			if (a.isNumeric()) {
				TaTa = TaTa + first.value(i) * first.value(i);
			}
		}
		for (int i = 0; i < first.numAttributes(); i++) {
			Attribute a = first.attribute(i);
			// Only use numeric attributes
			if (a.isNumeric()) {
				TbTb = TbTb + second.value(i) * second.value(i);
			}
		}

		if (stats != null)
			stats.incrCoordCount();

		double d = 1 - (TaTb / (TaTa + TbTb - TaTb));
		return d > cutOffValue ? Double.POSITIVE_INFINITY : d;
	}

	@Override
	public void postProcessDistances(double[] distances) {
	}

	@Override
	public void update(Instance ins) {

	}

	@Override
	public void clean() {
		data = null;
		range = new Range();
	}

}
