package weka.clusterers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.Vector;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.apache.commons.math3.util.Pair;
import org.apache.commons.math3.util.Precision;

import weka.core.Attribute;
import weka.core.Capabilities;
import weka.core.Capabilities.Capability;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.Option;
import weka.core.OptionHandler;
import weka.core.Randomizable;
import weka.core.Utils;

public class AntClusterer
		extends AbstractClusterer
		implements Randomizable, OptionHandler {

	// Settings
	@Getter
	@Setter
	private int gridSize;
	@Getter
	@Setter
	private int numAnts;
	@Getter
	@Setter
	private int iterations;
	@Getter
	@Setter
	private double fastProb;
	@Getter
	@Setter
	private int fastSpeed;
	@Getter
	@Setter
	private double alpha;
	// kp in paper
	@Getter
	@Setter
	private double pickupConstant;
	// kd in paper
	@Getter
	@Setter
	private double dropConstant;

	@Getter
	@Setter
	private boolean showVisualisation;
	@Getter
	@Setter
	private boolean showVisualisationAtEnd;

	// Random related variables
	private int seed;
	private RandomDataGenerator rand;

	// A list of clusters where each cluster is a set of instances
	private List<Set<ClusteringObject<Instance>>> clusters;

	// The grid of objects being clustered
	private ClusteringObject<Instance>[][] grid;

	// List of clustering ants
	private List<Ant> ants;

	private Attribute classAttr;

	private String dataSaveFile;

	public AntClusterer() {
		rand = new RandomDataGenerator(new MersenneTwister());
		this.gridSize = 150;
		this.numAnts = 300;
		this.iterations = 1000000000;
		this.fastProb = 0;
		this.fastSpeed = 6;
		this.alpha = 1;
		this.pickupConstant = 0.05;
		this.dropConstant = 0.8;

		this.dataSaveFile = "ant_clusterer_results.csv";

		this.showVisualisation = false;
		this.showVisualisationAtEnd = true;
	}

	@Override
	public void buildClusterer(Instances data) throws Exception {
		// Test data against capabilities
		getCapabilities().testWithFail(data);

		// Get class attribute
		for (int i = 0; i < data.numAttributes(); i++) {
			Attribute attr = data.attribute(i);
			if (attr.isNominal())
				classAttr = attr;
		}

		// Initialize variables
		grid = new ClusteringObject[gridSize][];
		for (int i = 0; i < gridSize; i++) {
			grid[i] = new ClusteringObject[gridSize];
		}
		ants = new ArrayList<>();
		clusters = new ArrayList<>();
		rand.reSeed(getSeed());

		// Initialize grid and ants
		int numInstances = data.numInstances();
		ClusteringObject.DistanceCache<Instance> cache = new ClusteringObject.DistanceCache<>(numInstances);
		for (int i = 0; i < numInstances; i++) {
			int x = 0;
			int y = 0;
			do {
				x = rand.nextInt(0, gridSize - 1);
				y = rand.nextInt(0, gridSize - 1);
			} while (grid[x][y] != null);
			grid[x][y] = new WekaClusteringObject(i, x, y, data.instance(i), cache);
		}

		for (int i = 0; i < numAnts; i++) {
			int x = rand.nextInt(0, gridSize - 1);
			int y = rand.nextInt(0, gridSize - 1);
			int speed;
			if (rand.nextUniform(0, 1) < fastProb) {
				speed = fastSpeed;
			} else {
				speed = 1;
			}
			ants.add(new Ant(x, y, speed));
		}

		// Show visualisation

		AntClusteringVisualisation vis = null;
		if (showVisualisation)
			vis = new AntClusteringVisualisation(grid, ants);

		// Run clustering

		for (int i = 0; i < iterations; i++) {
			int antID = rand.nextInt(0, ants.size() - 1);
			Ant activeAnt = ants.get(antID);

			// Observe the properties of the ant
			int x = activeAnt.getX();
			int y = activeAnt.getY();
			int speed = activeAnt.getSpeed();

			// See if is dragging something
			boolean onSomething = grid[x][y] != null;
			boolean carryingObject = activeAnt.isCarryingObject();

			// Observe the world
			double pPick = 0;
			double pDrop = 0;
			if (onSomething && !carryingObject && !grid[x][y].isBeingCarried()) {
				pPick = pPick(x, y);
			}
			if (carryingObject) {
				pDrop = pDrop(x, y);
			}

			// Do action: pickup
			double chance = rand.nextUniform(0, 1);
			if (chance < pPick) {
				activeAnt.setCarryingObject(true);
				carryingObject = true;
				grid[x][y].setBeingCarried(true);
			}

			// Do action: drop
			chance = rand.nextUniform(0, 1);
			if (chance < pDrop) {
				activeAnt.setCarryingObject(false);
				carryingObject = false;
				grid[x][y].setBeingCarried(false);
			}

			// Do action: walk
			int newX = x;
			int newY = y;
			for (int j = 0; j < speed; j++) {
				// calculate possible moves
				List<Neighbourhood> possibleMoves = Neighbourhood.getPossibleMoves(newX, newY, carryingObject, grid);
				// Ant can not move
				if (possibleMoves.isEmpty())
					break;
				// Move
				Neighbourhood move = possibleMoves.get(rand.nextInt(0, possibleMoves.size() - 1));
				newX += move.getDx();
				newY += move.getDy();
			}

			// Update the ant
			if (x != newX || y != newY)
				moveAnt(activeAnt, newX, newY);

			// Update visualisation
			if (showVisualisation) {
				vis.update();
				Thread.sleep(1);
			}

		}

		if (showVisualisationAtEnd)
			vis = new AntClusteringVisualisation(grid, ants);

		// Detect clusters (flood-fill)
		List<ClusteringObject<Instance>> unclustered = new ArrayList<>();
		for (ClusteringObject<Instance>[] row : grid) {
			for (ClusteringObject<Instance> obj : row) {
				if (obj != null)
					unclustered.add(obj);
			}
		}

		while (!unclustered.isEmpty()) {
			ClusteringObject<Instance> obj = unclustered.get(0);
			Set<ClusteringObject<Instance>> cluster = floodFill(obj.getX(), obj.getY());
			unclustered.removeAll(cluster);
			clusters.add(cluster);
		}

		if (dataSaveFile != null)
			writeResults();
	}

	/**
	 * Generate one cluster by flood-fill
	 *
	 * @param startX
	 *            starting X
	 * @param startY
	 *            starting Y
	 * @return A cluster
	 */
	private Set<ClusteringObject<Instance>> floodFill(int startX, int startY) {
		Queue<Pair<Integer, Integer>> fringe = new LinkedList<>();
		Set<ClusteringObject<Instance>> cluster = new HashSet<>();

		fringe.add(new Pair<>(startX, startY));
		while (!fringe.isEmpty()) {
			Pair<Integer, Integer> current = fringe.poll();

			int x = current.getFirst(), y = current.getSecond();

			ClusteringObject<Instance> obj = grid[x][y];
			if (obj != null) {
				if (!cluster.contains(obj)) {
					cluster.add(obj);
					for (Neighbourhood n : Neighbourhood.values()) {
						if (n.canMoveTo(x, y, grid.length, grid[0].length)) {
							fringe.add(new Pair<>(x + n.getDx(), y + n.getDy()));
						}
					}
				}
			}
		}

		return cluster;
	}

	/**
	 * Move an ant to the given coordinates
	 *
	 * @param ant
	 *            The ant to move
	 * @param newX
	 *            The x coordinate to move to
	 * @param newY
	 *            The y coordinate to move to
	 */
	private void moveAnt(Ant ant, int newX, int newY) {

		// Move the object if an ant is carrying one
		if (ant.isCarryingObject()) {
			// target location must be empty
			assert grid[newX][newY] == null : "Target location is not empty";

			// Source location must not be empty
			int oldX = ant.getX();
			int oldY = ant.getY();
			assert grid[oldX][oldY] != null : "No object a source location";

			ClusteringObject<Instance> moving = grid[oldX][oldY];
			grid[newX][newY] = moving;
			grid[oldX][oldY] = null;

			moving.setX(newX);
			moving.setY(newY);
		}

		assert newX < grid.length && newX >= 0 && newY < grid[0].length && newY >= 0 : "Incorrect new coordinates";

		ant.setX(newX);
		ant.setY(newY);
	}

	private double pPick(int x, int y) {
		double frac = pickupConstant / (pickupConstant + objectDensity(x, y));
		return frac * frac;
	}

	private double pDrop(int x, int y) {
		double fi = objectDensity(x, y);
		return fi < dropConstant ? 2 * fi : 1;
	}

	/**
	 * f(i) from the paper
	 *
	 * @param x
	 *            X coordinate
	 * @param y
	 *            Y coordinate
	 * @return A measure of density in the local neighbourhood
	 */
	private double objectDensity(int x, int y) {
		ClusteringObject<Instance> center = grid[x][y];

		double sum = 0;
		Neighbourhood[] neighbours = Neighbourhood.values();
		for (Neighbourhood dir : neighbours) {
			ClusteringObject<Instance> neighbour = dir.objectAt(x, y, grid);
			if (neighbour != null)
				sum += 1 - center.getDistance(neighbour) / alpha;
		}
		return sum > 0 ? sum / neighbours.length : 0;
	}

	@Override
	public double[] distributionForInstance(Instance instance) throws Exception {
		// Find the instance in the clusters by going through all clusters and seeing which cluster it is in
		double[] clusterScore = new double[clusters.size()];

		int cntr = 0;
		for (int i = 0; i < clusters.size(); i++) {
			Set<ClusteringObject<Instance>> cluster = clusters.get(i);
			for (ClusteringObject<Instance> obj : cluster) {
				boolean equal = true;
				for (int j = 0; j < obj.getData()
										.numAttributes(); i++) {
					equal &= Precision.equals(obj.getData()
													.value(i), instance.value(i));
				}
				if (equal) {
					clusterScore[i] = 1;
					cntr++;
					break;
				}
			}
		}

		// Total probability should be 1
		assert Precision.equals(Arrays.stream(clusterScore)
										.sum(), 1.0) : "Probability of being in a cluster does not sum to 1";

		return clusterScore;
	}

	@Override
	public int numberOfClusters() throws Exception {
		return clusters.size();
	}

	@Override
	public Capabilities getCapabilities() {
		Capabilities result = super.getCapabilities();

		result.enable(Capability.NUMERIC_ATTRIBUTES);

		result.enable(Capability.NO_CLASS);

		return result;
	}

	@Override
	public void setSeed(int seed) {
		this.seed = seed;
	}

	@Override
	public int getSeed() {
		return seed;
	}

	@Override
	public Enumeration listOptions() {
		Vector options = new Vector();

		options.add(new Option("Grid Size", "G", 1, "-G <num>"));
		options.add(new Option("Num Ants", "A", 1, "-A <num>"));
		options.add(new Option("Num Iterations", "I", 1, "-I <num>"));
		options.add(new Option("Fast probability", "F", 1, "-F <num>"));
		options.add(new Option("Fast speed", "S", 1, "-S <num>"));
		options.add(new Option("Alpha", "C", 1, "-C <num>"));
		options.add(new Option("Pickup Constant", "U", 1, "-U <num>"));
		options.add(new Option("Drop Constant", "D", 1, "-D <num>"));
		options.add(new Option("Show Visualisation", "V", 0, "-V"));
		options.add(new Option("Show Visualisation at end", "E", 0, "-E"));
		options.add(new Option("Data save filename", "Y", 1, "-Y <file>"));
		options.add(new Option("Random Seed", "R", 1, "-R <num>"));

		return options.elements();
	}

	@Override
	public void setOptions(String[] options) throws Exception {

		String optionString;

		optionString = Utils.getOption('G', options);
		if (optionString.length() != 0)
			setGridSize(Integer.parseInt(optionString));

		optionString = Utils.getOption('A', options);
		if (optionString.length() != 0)
			setNumAnts(Integer.parseInt(optionString));

		optionString = Utils.getOption('I', options);
		if (optionString.length() != 0)
			setIterations(Integer.parseInt(optionString));

		optionString = Utils.getOption('F', options);
		if (optionString.length() != 0)
			setFastProb(Double.parseDouble(optionString));

		optionString = Utils.getOption('S', options);
		if (optionString.length() != 0)
			setFastSpeed(Integer.parseInt(optionString));

		optionString = Utils.getOption('C', options);
		if (optionString.length() != 0)
			setAlpha(Double.parseDouble(optionString));

		optionString = Utils.getOption('U', options);
		if (optionString.length() != 0)
			setPickupConstant(Double.parseDouble(optionString));

		optionString = Utils.getOption('D', options);
		if (optionString.length() != 0)
			setDropConstant(Double.parseDouble(optionString));

		optionString = Utils.getOption('Y', options);
		if (optionString.length() != 0)
			dataSaveFile = optionString;

		optionString = Utils.getOption('R', options);
		if (optionString.length() != 0)
			setSeed(Integer.parseInt(optionString));

		showVisualisation = Utils.getFlag('V', options);
		showVisualisationAtEnd = Utils.getFlag('E', options);
	}

	@Override
	public String[] getOptions() {
		ArrayList<String> options = new ArrayList<>();

		options.add("-G");
		options.add(Integer.toString(gridSize));

		options.add("-A");
		options.add(Integer.toString(numAnts));

		options.add("-I");
		options.add(Integer.toString(iterations));

		options.add("-F");
		options.add(Double.toString(fastProb));

		options.add("-S");
		options.add(Integer.toString(fastSpeed));

		options.add("-C");
		options.add(Double.toString(alpha));

		options.add("-U");
		options.add(Double.toString(pickupConstant));

		options.add("-D");
		options.add(Double.toString(dropConstant));

		options.add("-Y");
		options.add(dataSaveFile);

		options.add("-R");
		options.add(Integer.toString(seed));

		if (showVisualisation)
			options.add("-V");

		if (showVisualisationAtEnd)
			options.add("-E");
		return options.toArray(new String[options.size()]);
	}

	public void writeResults() throws IOException {
		PrintWriter writer = new PrintWriter(dataSaveFile);

		int numClusters = classAttr.numValues();
		String[] possibleClasses = new String[numClusters];
		for (int i = 0; i < numClusters; i++)
			possibleClasses[i] = classAttr.value(i);

		writer.print("index");
		for (String c : possibleClasses)
			writer.print("," + c);
		writer.println();

		for (int i = 0; i < clusters.size(); i++) {
			Set<ClusteringObject<Instance>> cluster = clusters.get(i);
			int[] classCounts = new int[numClusters];

			for (ClusteringObject<Instance> obj : cluster) {
				classCounts[(int) Math.round(obj.getData()
												.value(classAttr))]++;
			}

			writer.print(i);
			for (int count : classCounts)
				writer.print("," + count);
			writer.println();
		}

		writer.close();
	}

	@AllArgsConstructor
	private enum Neighbourhood {
		NORTH(0, -1),
		EAST(1, 0),
		SOUTH(0, 1),
		WEST(-1, 0);

		@Getter
		private int dx, dy;

		/**
		 * Get the object next to the given coordinates in the direction this method is called on
		 *
		 * @param x
		 *            X coordinate
		 * @param y
		 *            Y coordinate
		 * @return The object or null
		 */
		public ClusteringObject<Instance> objectAt(int x, int y, ClusteringObject<Instance>[][] grid) {
			if (canMoveTo(x, y, grid.length, grid[0].length)) {
				return grid[x + dx][y + dy];
			}
			return null;
		}

		/**
		 * @param x
		 *            Origin x
		 * @param y
		 *            Origin y
		 * @return True if ant can move in this direction given its origin
		 */
		public boolean canMoveTo(int x, int y, int gridWidth, int gridHeight) {
			return x + dx >= 0 && x + dx < gridWidth && y + dy >= 0 && y + dy < gridHeight;
		}

		/**
		 * Get a list of possible moves for an ant
		 *
		 * @param x
		 *            Origin X
		 * @param y
		 *            Origin Y
		 * @param isCarryingObject
		 *            Whether an ant is carrying an object
		 * @param grid
		 *            The grid
		 * @return A list of possible moves
		 */
		public static List<Neighbourhood> getPossibleMoves(int x, int y, boolean isCarryingObject, ClusteringObject<Instance>[][] grid) {
			List<Neighbourhood> possibleMoves = new ArrayList<AntClusterer.Neighbourhood>();

			for (Neighbourhood dir : values()) {
				if (dir.canMoveTo(x, y, grid.length, grid[0].length) && (grid[x + dir.dx][y + dir.dy] == null || !isCarryingObject))
					possibleMoves.add(dir);
			}
			return possibleMoves;
		}
	}

	public static void main(String[] args) throws IOException {
		runClusterer(new AntClusterer(), args);
	}
}
