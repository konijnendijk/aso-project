package weka.clusterers;

import org.apache.commons.math3.util.FastMath;

import weka.core.Attribute;
import weka.core.Instance;

public class WekaClusteringObject
		extends ClusteringObject<Instance> {

	public WekaClusteringObject(int index, int x, int y, Instance data, weka.clusterers.ClusteringObject.DistanceCache<Instance> cache) {
		super(index, x, y, data, cache, false);
	}

	/**
	 * Calculates the euclidean distance
	 */
	@Override
	protected double calculateDistance(ClusteringObject<Instance> other) {
		assert data.numAttributes() == other.data.numAttributes() : "This and other do not have the same number of attributes";
		
                double TaTa = 0;
                double TaTb = 0;
                double TbTb = 0;          
                for (int i=0; i<data.numAttributes(); i++){
			Attribute a = data.attribute(i);
			// Only use numeric attributes
			if (a.isNumeric()) {
				TaTb = TaTb + other.data.value(i) * data.value(i);
			}
		}
                for (int i=0; i<data.numAttributes(); i++){
			Attribute a = data.attribute(i);
			// Only use numeric attributes
			if (a.isNumeric()) {
				TaTa = TaTa + data.value(i) * data.value(i);
			}
		}
                for (int i=0; i<data.numAttributes(); i++){
			Attribute a = data.attribute(i);
			// Only use numeric attributes
			if (a.isNumeric()) {
				TbTb = TbTb + other.data.value(i) * other.data.value(i);
			}
		}
		return 1-(TaTb/(TaTa + TbTb - TaTb));
	}

}
