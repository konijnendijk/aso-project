package weka.clusterers;

import java.io.Serializable;
import java.util.Arrays;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * @param <T> The type of the actual object being clustered
 */
@AllArgsConstructor
public abstract class ClusteringObject<T>
		implements Serializable {
    @Getter
    private int index;

    @Setter
    @Getter
    private int x, y;

    @Getter
	protected T data;

    private DistanceCache<T> cache;

	@Getter
	@Setter
	private boolean isBeingCarried;

    public double getDistance(ClusteringObject<T> other) {
        return cache.getDistance(this, other);
    }

    /**
	 * Calculates the euclidean distance from one object to the other
	 *
	 * @param other
	 *            The object to calculate the distance to
	 * @return The distance from this to other
	 */
    protected double calculateDistance(ClusteringObject<T> other) {
        return 0;
    }

    /**
     * Wrapper for a double[][] caching distances between {@link ClusteringObject ClusteringObjects} by index.
     */
	public static class DistanceCache<O>
			implements Serializable {
        private double[][] cache;

        public DistanceCache(int size) {
            cache = new double[size][];
            for (int i = 0; i < size; i++) {
				cache[i] = new double[size];
                // -1 if no entry exists in the cache yet
                Arrays.fill(cache[i], -1);
            }
        }

        /**
         * Get the distance between two objects from the cache or calculate it and store it in the cache
         */
        public double getDistance(ClusteringObject<O> obj1, ClusteringObject<O> obj2) {
            int index1 = obj1.getIndex();
            int index2 = obj2.getIndex();
            // Get the distance from the cache
            double distance = cache[index1][index2];
            // Calculate and store if it wasn't in the cache yet
            if (distance == -1) {
                distance = obj1.calculateDistance(obj2);
                cache[index1][index2] = distance;
                cache[index2][index1] = distance;
            }
            return distance;
        }
    }
}
