package weka.clusterers;

import weka.core.Attribute;
import weka.core.Instance;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class AntClusteringVisualisation {

	Color[] COLOURS= new Color[]{Color.BLUE, Color.GREEN, Color.RED, Color.MAGENTA, Color.YELLOW};

	private JFrame frame;
	private GridVisualisation panel;

	
	private ClusteringObject<Instance>[][] grid;
	private List<Ant> ants;


	public AntClusteringVisualisation(ClusteringObject<Instance>[][] grid, List<Ant> ant) {
		this.grid = grid;
		this.ants = ant;

		frame = new JFrame();

		frame.setSize(500, 500);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		panel = new GridVisualisation();
		frame.getContentPane()
				.add(panel);

		frame.setVisible(true);
	}

	public void update() {
		panel.repaint();
	}

	private class GridVisualisation
			extends JPanel {

		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);

			Graphics2D g2d = (Graphics2D) g;

			Dimension size = getSize();

			int gridWidth = grid.length;
			int gridHeight = grid[0].length;
			double cellWidth = size.getWidth() / gridWidth, cellHeight = size.getHeight() / gridHeight;

			g2d.setColor(Color.BLUE);

			// Clustering Objects
			for (int x = 0; x < gridWidth; x++) {
				for (int y = 0; y < gridHeight; y++) {
					ClusteringObject<Instance> obj = grid[x][y];
					if (obj != null) {

						Instance data = obj.getData();
						for (int i =0; i<data.numAttributes(); i++){
							Attribute attribute = data.attribute(i);
							if (attribute.isNominal()){
								int index = (int) Math.round(data.value(attribute));
								g2d.setColor(COLOURS[index]);
								break;
							}
						}

						Shape rect = new Rectangle2D.Double(x * cellWidth, y * cellHeight, cellWidth, cellHeight);
						g2d.fill(rect);
					}
				}
			}

			g2d.setColor(Color.BLACK);

			// Ants
			for (Ant a : ants) {
				Shape circle = new Ellipse2D.Double(a.getX() * cellWidth, a.getY() * cellHeight, cellWidth, cellHeight);
				g2d.fill(circle);
			}

		}
	}

}
