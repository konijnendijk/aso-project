package weka.clusterers;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * Class representing a clustering ant
 */
@Getter
@Setter
public class Ant
		implements Serializable {

    // Current ant location
    private int x,y, speed;

    // Whether an ant is carrying an object
    private boolean carryingObject;

    // TODO add fields like speed, etc.


    public Ant(int x, int y, int speed) {
        this.x = x;
        this.y = y;
        this.speed = speed;
    }


}
